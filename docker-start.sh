#~/bin/bash

version=${1:-latest}

docker run -it --rm -p 5050:8080 cognitivemedicine/eps:$version
